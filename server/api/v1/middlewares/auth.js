//Import JWT
const Jwt = require("jsonwebtoken");
//Import Settings
const { jwtSignature } = require("./../../../config/adaptor");
//Import Response Util
const Response = require("../utils/response");
class Auth {
  constructor() {}
  //JWT - Authentcation
  static jwtAuth = async (ctx, next) => {
    const authHeader = ctx.headers.authorization;
    //If No Auth token
    if (!authHeader) {
      return Response.unauthorized(ctx, {
        statusCode: 401,
        code: 41,
        msg: "No Token Provided",
      });
    }
    //Split token
    const token = authHeader.split(" ")[1]; //Authorization: Bearer token
    if (!token || token === "") {
      return Response.unauthorized(ctx, {
        statusCode: 401,
        code: 41,
        msg: "No Token Provided",
      });
    }
    try {
      const decoded = await Jwt.verify(token, jwtSignature.accessSecret);
      //Pass on the user to next controller
      ctx.state.user = decoded;
      //Next
      await next();
    } catch (err) {
      return Response.unauthorized(ctx, {
        statusCode: 401,
        code: 41,
        msg: "Unable to verify your credentials",
      });
    }
  };
}
//Export
module.exports = Auth;
