"use strict";
//Import Schema
const Schema = require("./Schema");

module.exports = (sequelize, DataTypes) => {
  const userSchema = Schema(DataTypes).Schema.User;
  const User = sequelize.define("user", userSchema);
  User.associate = function (models) {
    // associations can be defined here
  };
  return User;
};
