module.exports = (input = {}) => {
  const Schema = {
    User: {
      id: {
        type: input.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: input.STRING,
      email: input.STRING,
      mobile: input.STRING,
      password: input.STRING,
      address: input.STRING,
      isAdmin: input.STRING,
      createdAt: {
        type: input.DATE,
        allowNull: false,
      },
      updatedAt: {
        type: input.DATE,
        allowNull: false,
      },
    },
  };
  //User Schema final
  const { password, ...UserSchema } = Schema.User;
  //Return
  return {
    Schema,
    UserSchema,
  };
};
