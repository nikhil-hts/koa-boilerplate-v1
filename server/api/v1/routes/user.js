"use strict";
//Import Koa Router
const Router = require("koa-router");
//Instantiate Router
const router = new Router();
//Import Controller
const Controller = require("./../controllers/user");
//Import Auth - Middleware
const Auth = require("./../middlewares/auth");
/*
 
! User Routes

*/
//Get local Users
router.get("/", Auth.jwtAuth, Controller.getAllUsers);
//Add local User
router.post("/", Controller.addUser);
//Export
module.exports = router;
