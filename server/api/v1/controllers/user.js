//Import Axios
const Axios = require("axios");
//Import JWT
const Jwt = require("jsonwebtoken");
//Import Joi
const Joi = require("@hapi/joi");
//Import Bcrypt
const Bcrypt = require("bcrypt");
//Import Sequelize
const Sequelize = require("sequelize");
//User Schema
const Schema = require("./../db/models/Schema");
//Import Settings
const { jwtSignature } = require("./../../../config/adaptor");
//Import PG Models
const DB = require("./../db/models/pg");

//Import Response Util
const Response = require("../utils/response");
//Auth Controller
module.exports = class AuthHandler {
  constructor() {}
  //All Users
  static getAllUsers = async (ctx, next) => {
    let selectFields = [];
    //Get User Schema
    const schemaObj = Schema().UserSchema;
    //Query field implementation
    if (ctx.query && ctx.query.fields) {
      const fields = ctx.query.fields;
      let tmpFields = fields.split(",");
      for (let i = 0; i < tmpFields.length; i++) {
        if (tmpFields[i].trim() in schemaObj) {
          //Push
          selectFields.push(tmpFields[i].trim());
        }
      }
    }
    //If no field provided
    if (selectFields.length < 1) {
      for (const property in schemaObj) {
        selectFields.push(property);
      }
    }
    try {
      // Check if user exists
      const users = await DB.user.findAll(
        {
          attributes: selectFields,
        },
        { raw: true }
      );
      if (users.length > 0) {
        return Response.success(ctx, {
          statusCode: 200,
          code: 20,
          msg: "Records Found!",
          data: users,
        });
      } else {
        return Response.success(ctx, {
          statusCode: 200,
          code: 20,
          msg: "No Records Found!",
        });
      }
    } catch (err) {
      // console.log(err);
      return Response.error(ctx, {
        statusCode: 500,
        code: 50,
        msg: "Internal Error",
        error: err,
      });
    }
  };

  //Add Users
  static addUser = async (ctx, next) => {
    //Get Input
    const { name, email, mobile, address, password } = ctx.request.body;
    if (!name || !email || !password || !mobile || !address) {
      return Response.badRequest(ctx, {
        code: 40,
        msg: "Please provide valid data !",
      });
    }
    try {
      const Op = Sequelize.Op;
      // Check if user exists
      const checkUser = await DB.user.findAll({
        where: {
          [Op.or]: [{ email }, { mobile }],
        },
      });
      if (checkUser.length < 1) {
        //Hash the password
        const hash = Bcrypt.hashSync(password, 10);
        // Create a new user
        const newUser = await DB.user.create({
          name,
          email,
          mobile,
          address,
          isAdmin: false,
          password: hash,
        });
        //Remove password
        // const { password, ...data } = newUser.dataValues;
        return Response.created(ctx, {
          code: 21,
          msg: "User Added!",
          data: {
            id: newUser.id,
            name: newUser.name,
            email: newUser.email,
            mobile: newUser.mobile,
            address: newUser.address,
          },
        });
      } else {
        return Response.conflict(ctx, {
          code: 20,
          msg: "User already exits!",
        });
      }
    } catch (err) {
      console.log(err);
      return Response.error(ctx, {
        statusCode: 500,
        code: 50,
        msg: "Internal Error",
        error: err,
      });
    }
  };
};
