module.exports = {
  pgdbHost:
    process.env.SFM_PGDB_HOST || process.env.PGDB_HOST || "192.168.99.100",
  pgdbPort: process.env.SFM_PGDB_PORT || process.env.PGDB_PORT || "5432",
  pgdbIsAuth:
    process.env.SFM_PGDB_IS_AUTH || process.env.PGDB_IS_AUTH || "true",
  pgdbUsername:
    process.env.SFM_PGDB_USERNAME || process.env.PGDB_USERNAME || "postgres",
  pgdbPassword:
    process.env.SFM_PGDB_PASSWORD || process.env.PGDB_PASSWORD || "password",

  pgDbName: process.env.PGDB_NAME || "test-app",

  appPort: process.env.SFM_API_PORT || 3032,
  appHost: process.env.SFM_API_HOST || "0.0.0.0",

  appEnv: process.env.NODE_ENV || process.env.SFM_API_ENV || "dev",
  appLog: process.env.SFM_API_LOG || "dev",

  accessTokenSecret:
    process.env.SFM_API_ACCESS_TOKEN_SECRET ||
    "F4AE2797BA2DE610139037BE30173DCEEAFBB8ABB37CE60B81FD0BEC738F646A",
  refreshTokenSecret:
    process.env.SFM_API_REFRESH_TOKEN_SECRET ||
    "EFBF25E19309E46E1640E32322F56398D9D9726FD202DE0DCF18F199C6CE923B",
};
