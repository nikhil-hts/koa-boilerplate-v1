# Koa Boilerplate

( Koa Boilerplate )

## NPM Library Used

- Koa
- Postgres + Sequelizer
- Asynchronous Functions (Async/Await)

## Getting Started

```shell
$ git clone git@gitlab.com:nikhil-hts/koa-boilerplate-v1.git
$ cd sfm-apis
```

## Project structure

Here's the default structure for your project files.

- **server**
  - **config**
    - adaptor.js (Config Adaptor)
  - **api**
    - **v1**
      - **controllers**
      - **routes**
      - **services**
      - **db**
      - **utils**
- server.js (Server Configuration)
- index.js (Server Configuration)

# Environment variables

(To begin with, we have to define some environment variables)

| Name     | App Specific | Default | Required | Description | Valid | Notes |
| -------- | ------------ | ------- | :------: | ----------- | :---: | ----- |
| NODE_ENV |              | dev     |   Yes    |             |       |       |

## Running

Install dependencies

```
npm install
```

Run Application in Development Env

```
$ npm run start:dev
```

Run Application in Production Env

```
$ npm run start:prod

or

$ npm start
```
